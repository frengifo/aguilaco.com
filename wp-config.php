<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'db_aguila');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 's0p0rtex1');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'BUR%-Zbv}4Ji^#FAYc&#{ j{@9&L!!I /$IfOMf2>bND}@pt(.i6D5+OTh6+Y}AR');
define('SECURE_AUTH_KEY',  'ul^&+mZ=*#XVZ@>$J1;aU4Pp8.;(ix{&sdsNu[p&/i2!Eui%IiPvI*L8ckPtd?T1');
define('LOGGED_IN_KEY',    '$)s:-IT*#cH.^%^T<0{?*9e-utD%kk;4U^#$M#;V#MU0l)-SI?Wl>@e]_rb0kX]a');
define('NONCE_KEY',        'gF)83]v#r1YaM^>`2gG2iN@dgOa[7rQ)+yFdS98:H8BTtTL:Vi`})@tvY_IE:T@<');
define('AUTH_SALT',        'DODqy:#t?:#DX)h@_{W%FkG!8=YNL D$v.;!>pb Reg.?3aN9B%_E<OB(X$Wm.0N');
define('SECURE_AUTH_SALT', 'oO,OAeUcB=3}R-uO.BwV#G#Rl/bLy:GC$uOLyo`yqbGdoi(* A#duHj2oG:*G./f');
define('LOGGED_IN_SALT',   '8`Kxe~p)$a8=M<(|od2]u-}BYNgfcV_%J{,sUM~!5#7k-LMKhTDHJVb6.Y,=4g|R');
define('NONCE_SALT',       'pdXbaY}8.5U)OQz&Q@0+`6tSY5X]><4+6{&ENE-BG>xhurUw$9!S@tFWt=wW9zt]');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wpag_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

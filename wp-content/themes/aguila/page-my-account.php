<?php get_header( ); ?>
<?php while ( have_posts() ) : the_post(); ?>
<section class="heading my-cart" style="background:url('<?php echo get_template_directory_uri(); ?>/img/back-my-account.png') no-repeat 50% 50% #000;">
	<div class="container">
		<div class="row">
			<div class="col-md-12" >
				<h1>&nbsp;</h1>
				
				<p>&nbsp;</p>
			</div>
		</div>
	</div>

</section>
<section class="my-account">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<?php //is_user_logged_in() ?>
				<?php if (!is_user_logged_in()): ?>
					<h1><i>LOG IN</i></h1>
					<p>MANAGE YOUR ACCOUNT AND SEE YOUR ORDERS</p>
					<p>Contrary to popular belief, Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					consequat. Duis aute irure dolor in reprehenderit in voluptate velit ess </p>
					<p>&nbsp;</p>
				<?php endif ?>
				<?php the_content(); ?>
			</div>
		</div>
	</div>
</section>

<?php endwhile; // end of the loop. ?>

<?php get_footer(); ?>
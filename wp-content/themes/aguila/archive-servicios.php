<?php get_header(); ?>
<section class="content-page servicios">
        
    <section class="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <?php if ( function_exists('yoast_breadcrumb') ) {
                            yoast_breadcrumb('
                            <p id="breadcrumbs" class="pull-right">','</p>
                            ');
                        }
                    ?>
                    <h2> <span><img src="<?php echo get_template_directory_uri() ?>/img/icon-servicios.png"></span> Servicios</h2>
                </div>
            </div>
        </div>
    </section>
    <section class="services">
        <div class="container">
            <div class="row">
                <div class="col-md-12 heading">

                    <?php 
                        $args = array(
                          'name'        => 'servicios',
                          'post_type'   => 'cabeceras',
                          'post_status' => 'publish',
                          'numberposts' => 1
                        );
                        $info_producto = get_posts($args);
                    ?>

                    <h2><?php echo $info_producto[0]->post_title; ?></h2>
                    <p><?php echo $info_producto[0]->post_excerpt; ?></p>

                </div>
            </div>
            <div class="row">
            <?php $cats = get_terms( 'tipo', array( 'hide_empty' => 0 ) );
                        
                if ( ! empty( $cats ) && ! is_wp_error( $cats ) ){
                
                    foreach ( $cats as $term ) { ?>
                    
                        
                        <article class="col-md-4 col-sm-4 col-xs-12 item">
                            <h2> <?php echo $term->name; ?></h2>
                            <div class="box">
                                <a href="<?php echo get_term_link($term); ?>">
                                <?php $servicios = new WP_Query( array(  'post_type' => 'servicios', 'tipo'=>$term->slug ) ); ?>
                                <?php while ( $servicios->have_posts() ) : $servicios->the_post(); ?>
                                        <img src="<?php the_field('icono'); ?>" alt="<?php the_title(); ?>" />
                                        <span><?php the_title(); ?></span>
                                <?php endwhile; ?>
                                </a>
                            </div>
                        </article>

                    <?php } ?>

            <?php } ?>
                

            </div>
        </div>                                  
    </section>
</section>
            
<?php get_footer(); ?>
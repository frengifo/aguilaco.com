<?php 
/**
 * Template Name: Contacto
 *
 */
?>
<?php get_header(); ?>

<section class="content-page contacto">
    <section id="map" style="height:450px"></section>
    <section class="contenido">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <?php echo do_shortcode( '[contact-form-7 id="189" title="Contacto"]' ); ?>

                </div>
                <div class="col-md-6 info">
                    <!--<h2>Gracias por contactarnos</h2>
                    <p>Déjanos un mensaje y en la brevedad atenderemos tus dudas con nuestro personal especializadp 24/7</p>

                    <h2>Información de Contacto</h2>
                    <h3>HORARIO DE ATENCIÓN</h3>
                    <p>
                        Lunes - Viernes <br>
                        9am - 6pm <br>
                        Sábados <br>
                        10am - 1pm
                    </p>
                    <h3>DIRECCIÓN</h3>
                    <p>PONER DIRECCIÓN</p>

                    <h3>EMAIL</h3>
                    <a href="#">correoscontacto@pertel.pe</a> &nbsp;-&nbsp; <a href="#">correoscontacto@pertel.pe</a>

                    <h3>TELÉFONOS</h3>
                    <p>987 654 321</p>-->
		    <?php the_content(); ?>

                </div>
            </div>
        </div>
    </section>
</section>
<script type="text/javascript"  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAHBq92Kt1SaSlSWIwyo7_8qhEnRIjq-74&libraries=geometry"></script>

<?php get_footer(); ?>
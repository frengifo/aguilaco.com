<?php get_header( ); ?>
<?php while ( have_posts() ) : the_post(); ?>

<section class="contact-page">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<img src="<?php echo get_template_directory_uri(); ?>/img/contact-map.png" alt="Aguila Co." />
				<ul>
					<li>
						<span class="marker"></span> Lorem Ipsum Office 12345 Stree name - California - USA
					</li>
					<li>
						<span class="tel"></span> +511 968 392 018
					</li>
					<li>
						<span class="email"></span> admin@aguila.co
					</li>
				</ul>
				<h2>Leave a message</h2>
				<p>If you have a questions, please feel free to contact us.</p>
				<p>You are important for us, so we will do our best to help you. if it's an engagemente, we'll take the extra measures to make it especial</p>

				<form method="post" action="#">
					<input type="email" name="email" placeholder="Email*">
					<input type="text" name="name" placeholder="Name*">
					<textarea name="message" placeholder="Your Review"></textarea>
					<label for="for"><input type="checkbox" name="sendyou" id="for"> Send a copy of this email to you </label><br>
					<button type="submit" class="send">Submit</button>
				</form>

			</div>
		</div>
	</div>
</section>

<?php endwhile; // end of the loop. ?>

<?php get_footer(); ?>
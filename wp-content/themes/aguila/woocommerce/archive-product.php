<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header( 'shop' ); ?>

<section class="heading white">
	<div class="container">
		<div class="row">
			<div class="col-md-12" style="background:url('<?php echo get_template_directory_uri(); ?>/img/heading-bracelets.png') no-repeat 100% 0% #000;">
				<h1>BRACELETS <br> <small>ELGANCE</small></h1>
			</div>
		</div>
	</div>

</section>

<section class="filter">
	
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<select class="pull-right">
					<option value="low">Low price</option>
					<option value="hight">Hight</option>
				</select>
				<article>
					<i>Filter by:</i> 
					<ul>
						<li><a href="#">Designers & Collections</a></li>
						<li><a href="#">Materials</a></li>
						<li><a href="#">Gemstones</a></li>
					</ul>
				</article>
			</div>
		</div>
	</div>

</section>

<section class="list-products">
	<div class="container">
		
		<div class="row">
			<div class="col-md-12">
				<div class="products row">
					<div class="col-md-6 col-sm-6 col-xs-12 full">
						<div class="box">
							
							<article class="desc">
								<h2>ELEGANCE</h2>
								<article>
									<p>An ancient icon its legacy on elegant jewelry<br>creations traced width sparkling</p>
								</article>
								
							</article>
							<img src="<?php echo get_template_directory_uri(); ?>/img/pic-category.png" alt="Collar Empote">

						</div>
					</div>
					<div class="col-md-3 col-sm-3 col-xs-6">
						<article>
							
							<img src="<?php echo get_template_directory_uri(); ?>/img/earings.png" alt="Collar Empote">
							<div class="hover">
								<div class="links">
									<a href="<?php echo site_url(); ?>/shop/lace-collar/" class="view-products">&nbsp;</a>
									<a href="#view-1" class="shop-product various">&nbsp;</a>
									<a href="#view-1" class="add-wishlist various">&nbsp;</a>
								</div>
							</div>

						</article>
						
						<h3>COLLAR EMPOTE</h3>
						<span class="price">$45.00</span>
						<div class="hidden">
							<div id="view-1">
								<div class="box-detail">
										
									<div class="box">
										<div class="full">
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/earings.png" alt="Collar Empote">
											</div>
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/braceletes.png" alt="Collar Empote">
											</div>
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/ring.png" alt="Collar Empote">
											</div>
										</div>
										<div class="thumb">
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/earings-thumb.png" alt="Collar Empote">
											</div>
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/braceletes-thumb.png" alt="Collar Empote">
											</div>
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/ring-thumb.png" alt="Collar Empote">
											</div>

										</div>
									</div>
									<div class="box right">
										<h3>ARETES WOO 23</h3>
										<article>
											<p>
												Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
												tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
												quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
											</p>
										</article>
										<p>
											<strong>Category:</strong> <span>Bracelets</span><br>
											<strong>ID:</strong> <span>1799</span>
										</p>

										<div class="price">
											$ 45.00
										</div>

										<form method="get" action="#">
					                    	<input type="number" name="cantidad" id="quantity" min="1" placeholder="Quantity" value="1">
					                    	<button type="button" class="add-to-cart">
					                    		<i class="fa fa-shopping-cart" aria-hidden="true"></i> AÑADIR A CARRITO
					                    	</button>
					                    	<a href="javascript:;" class="btn-wishlist">&nbsp;</a>
					                    </form>

					                    <p>
					                    	<a href="javascript:;">PRINT</a>  |  <a href="javascript:;" >SHARE</a>
					                    </p>
									</div>

								</div>
							</div>
						</div>
					</div>

					<div class="col-md-3 col-sm-3 col-xs-6">
						<article>
							
							<img src="<?php echo get_template_directory_uri(); ?>/img/braceletes.png" alt="Collar Empote">
							<div class="hover">
								<div class="links">
									<a href="<?php echo site_url(); ?>/shop/lace-collar/" class="view-products">&nbsp;</a>
									<a href="#view-2" class="shop-product various">&nbsp;</a>
									<a href="#view-2" class="add-wishlist various">&nbsp;</a>
								</div>
							</div>

						</article>
						
						<h3>COLLAR EMPOTE</h3>
						<span class="price">$45.00</span>
						<div class="hidden">
							<div id="view-2">
								<div class="box-detail">
										
									<div class="box">
										<div class="full">
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/earings.png" alt="Collar Empote">
											</div>
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/braceletes.png" alt="Collar Empote">
											</div>
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/ring.png" alt="Collar Empote">
											</div>
										</div>
										<div class="thumb">
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/earings-thumb.png" alt="Collar Empote">
											</div>
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/braceletes-thumb.png" alt="Collar Empote">
											</div>
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/ring-thumb.png" alt="Collar Empote">
											</div>

										</div>
									</div>
									<div class="box right">
										<h3>ARETES WOO 23</h3>
										<article>
											<p>
												Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
												tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
												quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
											</p>
										</article>
										<p>
											<strong>Category:</strong> <span>Bracelets</span><br>
											<strong>ID:</strong> <span>1799</span>
										</p>

										<div class="price">
											$ 45.00
										</div>

										<form method="get" action="#">
					                    	<input type="number" name="cantidad" id="quantity" min="1" placeholder="Quantity" value="1">
					                    	<button type="button" class="add-to-cart">
					                    		<i class="fa fa-shopping-cart" aria-hidden="true"></i> AÑADIR A CARRITO
					                    	</button>
					                    	<a href="javascript:;" class="btn-wishlist">&nbsp;</a>
					                    </form>

					                    <p>
					                    	<a href="javascript:;">PRINT</a>  |  <a href="javascript:;" >SHARE</a>
					                    </p>
									</div>

								</div>
							</div>
						</div>
					</div>

					<div class="col-md-3 col-sm-3 col-xs-6">
						<article>
							
							<img src="<?php echo get_template_directory_uri(); ?>/img/earings.png" alt="Collar Empote">
							<div class="hover">
								<div class="links">
									<a href="<?php echo site_url(); ?>/shop/lace-collar/" class="view-products">&nbsp;</a>
									<a href="#view-3" class="shop-product various">&nbsp;</a>
									<a href="#view-3" class="add-wishlist various">&nbsp;</a>
								</div>
							</div>

						</article>
						
						<h3>COLLAR EMPOTE</h3>
						<span class="price">$45.00</span>
						<div class="hidden">
							<div id="view-3">
								<div class="box-detail">
										
									<div class="box">
										<div class="full">
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/earings.png" alt="Collar Empote">
											</div>
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/braceletes.png" alt="Collar Empote">
											</div>
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/ring.png" alt="Collar Empote">
											</div>
										</div>
										<div class="thumb">
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/earings-thumb.png" alt="Collar Empote">
											</div>
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/braceletes-thumb.png" alt="Collar Empote">
											</div>
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/ring-thumb.png" alt="Collar Empote">
											</div>

										</div>
									</div>
									<div class="box right">
										<h3>ARETES WOO 23</h3>
										<article>
											<p>
												Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
												tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
												quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
											</p>
										</article>
										<p>
											<strong>Category:</strong> <span>Bracelets</span><br>
											<strong>ID:</strong> <span>1799</span>
										</p>

										<div class="price">
											$ 45.00
										</div>

										<form method="get" action="#">
					                    	<input type="number" name="cantidad" id="quantity" min="1" placeholder="Quantity" value="1">
					                    	<button type="button" class="add-to-cart">
					                    		<i class="fa fa-shopping-cart" aria-hidden="true"></i> AÑADIR A CARRITO
					                    	</button>
					                    	<a href="javascript:;" class="btn-wishlist">&nbsp;</a>
					                    </form>

					                    <p>
					                    	<a href="javascript:;">PRINT</a>  |  <a href="javascript:;" >SHARE</a>
					                    </p>
									</div>

								</div>
							</div>
						</div>
					</div>

					<div class="col-md-3 col-sm-3 col-xs-6">
						<article>
							
							<img src="<?php echo get_template_directory_uri(); ?>/img/braceletes.png" alt="Collar Empote">
							<div class="hover">
								<div class="links">
									<a href="<?php echo site_url(); ?>/shop/lace-collar/" class="view-products">&nbsp;</a>
									<a href="#view-4" class="shop-product various">&nbsp;</a>
									<a href="#view-4" class="add-wishlist various">&nbsp;</a>
								</div>
							</div>

						</article>
						
						<h3>COLLAR EMPOTE</h3>
						<span class="price">$45.00</span>
						<div class="hidden">
							<div id="view-4">
								<div class="box-detail">
										
									<div class="box">
										<div class="full">
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/earings.png" alt="Collar Empote">
											</div>
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/braceletes.png" alt="Collar Empote">
											</div>
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/ring.png" alt="Collar Empote">
											</div>
										</div>
										<div class="thumb">
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/earings-thumb.png" alt="Collar Empote">
											</div>
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/braceletes-thumb.png" alt="Collar Empote">
											</div>
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/ring-thumb.png" alt="Collar Empote">
											</div>

										</div>
									</div>
									<div class="box right">
										<h3>ARETES WOO 23</h3>
										<article>
											<p>
												Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
												tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
												quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
											</p>
										</article>
										<p>
											<strong>Category:</strong> <span>Bracelets</span><br>
											<strong>ID:</strong> <span>1799</span>
										</p>

										<div class="price">
											$ 45.00
										</div>

										<form method="get" action="#">
					                    	<input type="number" name="cantidad" id="quantity" min="1" placeholder="Quantity" value="1">
					                    	<button type="button" class="add-to-cart">
					                    		<i class="fa fa-shopping-cart" aria-hidden="true"></i> AÑADIR A CARRITO
					                    	</button>
					                    	<a href="javascript:;" class="btn-wishlist">&nbsp;</a>
					                    </form>

					                    <p>
					                    	<a href="javascript:;">PRINT</a>  |  <a href="javascript:;" >SHARE</a>
					                    </p>
									</div>

								</div>
							</div>
						</div>
					</div>
					<div class="clear"></div>

					<div class="col-md-3 col-sm-3 col-xs-6">
						<article>
							
							<img src="<?php echo get_template_directory_uri(); ?>/img/earings.png" alt="Collar Empote">
							<div class="hover">
								<div class="links">
									<a href="<?php echo site_url(); ?>/shop/lace-collar/" class="view-products">&nbsp;</a>
									<a href="#view-5" class="shop-product various">&nbsp;</a>
									<a href="#view-5" class="add-wishlist various">&nbsp;</a>
								</div>
							</div>

						</article>
						
						<h3>COLLAR EMPOTE</h3>
						<span class="price">$45.00</span>
						<div class="hidden">
							<div id="view-5">
								<div class="box-detail">
										
									<div class="box">
										<div class="full">
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/earings.png" alt="Collar Empote">
											</div>
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/braceletes.png" alt="Collar Empote">
											</div>
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/ring.png" alt="Collar Empote">
											</div>
										</div>
										<div class="thumb">
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/earings-thumb.png" alt="Collar Empote">
											</div>
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/braceletes-thumb.png" alt="Collar Empote">
											</div>
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/ring-thumb.png" alt="Collar Empote">
											</div>

										</div>
									</div>
									<div class="box right">
										<h3>ARETES WOO 23</h3>
										<article>
											<p>
												Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
												tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
												quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
											</p>
										</article>
										<p>
											<strong>Category:</strong> <span>Bracelets</span><br>
											<strong>ID:</strong> <span>1799</span>
										</p>

										<div class="price">
											$ 45.00
										</div>

										<form method="get" action="#">
					                    	<input type="number" name="cantidad" id="quantity" min="1" placeholder="Quantity" value="1">
					                    	<button type="button" class="add-to-cart">
					                    		<i class="fa fa-shopping-cart" aria-hidden="true"></i> AÑADIR A CARRITO
					                    	</button>
					                    	<a href="javascript:;" class="btn-wishlist">&nbsp;</a>
					                    </form>

					                    <p>
					                    	<a href="javascript:;">PRINT</a>  |  <a href="javascript:;" >SHARE</a>
					                    </p>
									</div>

								</div>
							</div>
						</div>
					</div>

					<div class="col-md-3 col-sm-3 col-xs-6">
						<article>
							
							<img src="<?php echo get_template_directory_uri(); ?>/img/braceletes.png" alt="Collar Empote">
							<div class="hover">
								<div class="links">
									<a href="<?php echo site_url(); ?>/shop/lace-collar/" class="view-products">&nbsp;</a>
									<a href="#view-6" class="shop-product various">&nbsp;</a>
									<a href="#view-6" class="add-wishlist various">&nbsp;</a>
								</div>
							</div>

						</article>
						
						<h3>COLLAR EMPOTE</h3>
						<span class="price">$45.00</span>
						<div class="hidden">
							<div id="view-6">
								<div class="box-detail">
										
									<div class="box">
										<div class="full">
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/earings.png" alt="Collar Empote">
											</div>
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/braceletes.png" alt="Collar Empote">
											</div>
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/ring.png" alt="Collar Empote">
											</div>
										</div>
										<div class="thumb">
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/earings-thumb.png" alt="Collar Empote">
											</div>
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/braceletes-thumb.png" alt="Collar Empote">
											</div>
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/ring-thumb.png" alt="Collar Empote">
											</div>

										</div>
									</div>
									<div class="box right">
										<h3>ARETES WOO 23</h3>
										<article>
											<p>
												Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
												tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
												quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
											</p>
										</article>
										<p>
											<strong>Category:</strong> <span>Bracelets</span><br>
											<strong>ID:</strong> <span>1799</span>
										</p>

										<div class="price">
											$ 45.00
										</div>

										<form method="get" action="#">
					                    	<input type="number" name="cantidad" id="quantity" min="1" placeholder="Quantity" value="1">
					                    	<button type="button" class="add-to-cart">
					                    		<i class="fa fa-shopping-cart" aria-hidden="true"></i> AÑADIR A CARRITO
					                    	</button>
					                    	<a href="javascript:;" class="btn-wishlist">&nbsp;</a>
					                    </form>

					                    <p>
					                    	<a href="javascript:;">PRINT</a>  |  <a href="javascript:;" >SHARE</a>
					                    </p>
									</div>

								</div>
							</div>
						</div>
					</div>

					<div class="col-md-3 col-sm-3 col-xs-6">
						<article>
							
							<img src="<?php echo get_template_directory_uri(); ?>/img/earings.png" alt="Collar Empote">
							<div class="hover">
								<div class="links">
									<a href="<?php echo site_url(); ?>/shop/lace-collar/" class="view-products">&nbsp;</a>
									<a href="#view-7" class="shop-product various">&nbsp;</a>
									<a href="#view-7" class="add-wishlist various">&nbsp;</a>
								</div>
							</div>

						</article>
						
						<h3>COLLAR EMPOTE</h3>
						<span class="price">$45.00</span>
						<div class="hidden">
							<div id="view-7">
								<div class="box-detail">
										
									<div class="box">
										<div class="full">
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/earings.png" alt="Collar Empote">
											</div>
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/braceletes.png" alt="Collar Empote">
											</div>
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/ring.png" alt="Collar Empote">
											</div>
										</div>
										<div class="thumb">
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/earings-thumb.png" alt="Collar Empote">
											</div>
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/braceletes-thumb.png" alt="Collar Empote">
											</div>
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/ring-thumb.png" alt="Collar Empote">
											</div>

										</div>
									</div>
									<div class="box right">
										<h3>ARETES WOO 23</h3>
										<article>
											<p>
												Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
												tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
												quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
											</p>
										</article>
										<p>
											<strong>Category:</strong> <span>Bracelets</span><br>
											<strong>ID:</strong> <span>1799</span>
										</p>

										<div class="price">
											$ 45.00
										</div>

										<form method="get" action="#">
					                    	<input type="number" name="cantidad" id="quantity" min="1" placeholder="Quantity" value="1">
					                    	<button type="button" class="add-to-cart">
					                    		<i class="fa fa-shopping-cart" aria-hidden="true"></i> AÑADIR A CARRITO
					                    	</button>
					                    	<a href="javascript:;" class="btn-wishlist">&nbsp;</a>
					                    </form>

					                    <p>
					                    	<a href="javascript:;">PRINT</a>  |  <a href="javascript:;" >SHARE</a>
					                    </p>
									</div>

								</div>
							</div>
						</div>
					</div>

					<div class="col-md-3 col-sm-3 col-xs-6">
						<article>
							
							<img src="<?php echo get_template_directory_uri(); ?>/img/braceletes.png" alt="Collar Empote">
							<div class="hover">
								<div class="links">
									<a href="<?php echo site_url(); ?>/shop/lace-collar/" class="view-products">&nbsp;</a>
									<a href="#view-8" class="shop-product various">&nbsp;</a>
									<a href="#view-8" class="add-wishlist various">&nbsp;</a>
								</div>
							</div>

						</article>
						
						<h3>COLLAR EMPOTE</h3>
						<span class="price">$45.00</span>
						<div class="hidden">
							<div id="view-8">
								<div class="box-detail">
										
									<div class="box">
										<div class="full">
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/earings.png" alt="Collar Empote">
											</div>
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/braceletes.png" alt="Collar Empote">
											</div>
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/ring.png" alt="Collar Empote">
											</div>
										</div>
										<div class="thumb">
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/earings-thumb.png" alt="Collar Empote">
											</div>
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/braceletes-thumb.png" alt="Collar Empote">
											</div>
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/ring-thumb.png" alt="Collar Empote">
											</div>

										</div>
									</div>
									<div class="box right">
										<h3>ARETES WOO 23</h3>
										<article>
											<p>
												Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
												tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
												quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
											</p>
										</article>
										<p>
											<strong>Category:</strong> <span>Bracelets</span><br>
											<strong>ID:</strong> <span>1799</span>
										</p>

										<div class="price">
											$ 45.00
										</div>

										<form method="get" action="#">
					                    	<input type="number" name="cantidad" id="quantity" min="1" placeholder="Quantity" value="1">
					                    	<button type="button" class="add-to-cart">
					                    		<i class="fa fa-shopping-cart" aria-hidden="true"></i> AÑADIR A CARRITO
					                    	</button>
					                    	<a href="javascript:;" class="btn-wishlist">&nbsp;</a>
					                    </form>

					                    <p>
					                    	<a href="javascript:;">PRINT</a>  |  <a href="javascript:;" >SHARE</a>
					                    </p>
									</div>

								</div>
							</div>
						</div>
					</div>

					<div class="clear"></div>

					<div class="col-md-3 col-sm-3 col-xs-6">
						<article>
							
							<img src="<?php echo get_template_directory_uri(); ?>/img/earings.png" alt="Collar Empote">
							<div class="hover">
								<div class="links">
									<a href="<?php echo site_url(); ?>/shop/lace-collar/" class="view-products">&nbsp;</a>
									<a href="#view-9" class="shop-product various">&nbsp;</a>
									<a href="#view-9" class="add-wishlist various">&nbsp;</a>
								</div>
							</div>

						</article>
						
						<h3>COLLAR EMPOTE</h3>
						<span class="price">$45.00</span>
						<div class="hidden">
							<div id="view-9">
								<div class="box-detail">
										
									<div class="box">
										<div class="full">
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/earings.png" alt="Collar Empote">
											</div>
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/braceletes.png" alt="Collar Empote">
											</div>
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/ring.png" alt="Collar Empote">
											</div>
										</div>
										<div class="thumb">
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/earings-thumb.png" alt="Collar Empote">
											</div>
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/braceletes-thumb.png" alt="Collar Empote">
											</div>
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/ring-thumb.png" alt="Collar Empote">
											</div>

										</div>
									</div>
									<div class="box right">
										<h3>ARETES WOO 23</h3>
										<article>
											<p>
												Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
												tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
												quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
											</p>
										</article>
										<p>
											<strong>Category:</strong> <span>Bracelets</span><br>
											<strong>ID:</strong> <span>1799</span>
										</p>

										<div class="price">
											$ 45.00
										</div>

										<form method="get" action="#">
					                    	<input type="number" name="cantidad" id="quantity" min="1" placeholder="Quantity" value="1">
					                    	<button type="button" class="add-to-cart">
					                    		<i class="fa fa-shopping-cart" aria-hidden="true"></i> AÑADIR A CARRITO
					                    	</button>
					                    	<a href="javascript:;" class="btn-wishlist">&nbsp;</a>
					                    </form>

					                    <p>
					                    	<a href="javascript:;">PRINT</a>  |  <a href="javascript:;" >SHARE</a>
					                    </p>
									</div>

								</div>
							</div>
						</div>
					</div>

					<div class="col-md-3 col-sm-3 col-xs-6">
						<article>
							
							<img src="<?php echo get_template_directory_uri(); ?>/img/braceletes.png" alt="Collar Empote">
							<div class="hover">
								<div class="links">
									<a href="<?php echo site_url(); ?>/shop/lace-collar/" class="view-products">&nbsp;</a>
									<a href="#view-10" class="shop-product various">&nbsp;</a>
									<a href="#view-10" class="add-wishlist various">&nbsp;</a>
								</div>
							</div>

						</article>
						
						<h3>COLLAR EMPOTE</h3>
						<span class="price">$45.00</span>
						<div class="hidden">
							<div id="view-10">
								<div class="box-detail">
										
									<div class="box">
										<div class="full">
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/earings.png" alt="Collar Empote">
											</div>
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/braceletes.png" alt="Collar Empote">
											</div>
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/ring.png" alt="Collar Empote">
											</div>
										</div>
										<div class="thumb">
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/earings-thumb.png" alt="Collar Empote">
											</div>
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/braceletes-thumb.png" alt="Collar Empote">
											</div>
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/ring-thumb.png" alt="Collar Empote">
											</div>

										</div>
									</div>
									<div class="box right">
										<h3>ARETES WOO 23</h3>
										<article>
											<p>
												Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
												tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
												quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
											</p>
										</article>
										<p>
											<strong>Category:</strong> <span>Bracelets</span><br>
											<strong>ID:</strong> <span>1799</span>
										</p>

										<div class="price">
											$ 45.00
										</div>

										<form method="get" action="#">
					                    	<input type="number" name="cantidad" id="quantity" min="1" placeholder="Quantity" value="1">
					                    	<button type="button" class="add-to-cart">
					                    		<i class="fa fa-shopping-cart" aria-hidden="true"></i> AÑADIR A CARRITO
					                    	</button>
					                    	<a href="javascript:;" class="btn-wishlist">&nbsp;</a>
					                    </form>

					                    <p>
					                    	<a href="javascript:;">PRINT</a>  |  <a href="javascript:;" >SHARE</a>
					                    </p>
									</div>

								</div>
							</div>
						</div>
					</div>

					<div class="col-md-3 col-sm-3 col-xs-6">
						<article>
							
							<img src="<?php echo get_template_directory_uri(); ?>/img/earings.png" alt="Collar Empote">
							<div class="hover">
								<div class="links">
									<a href="<?php echo site_url(); ?>/shop/lace-collar/" class="view-products">&nbsp;</a>
									<a href="#view-11" class="shop-product various">&nbsp;</a>
									<a href="#" class="add-wishlist various ">&nbsp;</a>
								</div>
							</div>

						</article>
						
						<h3>COLLAR EMPOTE</h3>
						<span class="price">$45.00</span>
						<div class="hidden">
							<div id="view-11">
								<div class="box-detail">
										
									<div class="box">
										<div class="full">
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/earings.png" alt="Collar Empote">
											</div>
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/braceletes.png" alt="Collar Empote">
											</div>
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/ring.png" alt="Collar Empote">
											</div>
										</div>
										<div class="thumb">
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/earings-thumb.png" alt="Collar Empote">
											</div>
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/braceletes-thumb.png" alt="Collar Empote">
											</div>
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/ring-thumb.png" alt="Collar Empote">
											</div>

										</div>
									</div>
									<div class="box right">
										<h3>ARETES WOO 23</h3>
										<article>
											<p>
												Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
												tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
												quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
											</p>
										</article>
										<p>
											<strong>Category:</strong> <span>Bracelets</span><br>
											<strong>ID:</strong> <span>1799</span>
										</p>

										<div class="price">
											$ 45.00
										</div>

										<form method="get" action="#">
					                    	<input type="number" name="cantidad" id="quantity" min="1" placeholder="Quantity" value="1">
					                    	<button type="button" class="add-to-cart">
					                    		<i class="fa fa-shopping-cart" aria-hidden="true"></i> AÑADIR A CARRITO
					                    	</button>
					                    	<a href="javascript:;" class="btn-wishlist">&nbsp;</a>
					                    </form>

					                    <p>
					                    	<a href="javascript:;">PRINT</a>  |  <a href="javascript:;" >SHARE</a>
					                    </p>
									</div>

								</div>
							</div>
						</div>
					</div>

					<div class="col-md-3 col-sm-3 col-xs-6">
						<article>
							
							<img src="<?php echo get_template_directory_uri(); ?>/img/braceletes.png" alt="Collar Empote">
							<div class="hover">
								<div class="links">
									<a href="<?php echo site_url(); ?>/shop/lace-collar/" class="view-products">&nbsp;</a>
									<a href="#view-12" class="shop-product various">&nbsp;</a>
									<a href="#view-12" class="add-wishlist various">&nbsp;</a>
								</div>
							</div>

						</article>
						
						<h3>COLLAR EMPOTE</h3>
						<span class="price">$45.00</span>
						<div class="hidden">
							<div id="view-12">
								<div class="box-detail">
										
									<div class="box">
										<div class="full">
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/earings.png" alt="Collar Empote">
											</div>
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/braceletes.png" alt="Collar Empote">
											</div>
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/ring.png" alt="Collar Empote">
											</div>
										</div>
										<div class="thumb">
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/earings-thumb.png" alt="Collar Empote">
											</div>
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/braceletes-thumb.png" alt="Collar Empote">
											</div>
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/ring-thumb.png" alt="Collar Empote">
											</div>

										</div>
									</div>
									<div class="box right">
										<h3>ARETES WOO 23</h3>
										<article>
											<p>
												Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
												tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
												quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
											</p>
										</article>
										<p>
											<strong>Category:</strong> <span>Bracelets</span><br>
											<strong>ID:</strong> <span>1799</span>
										</p>

										<div class="price">
											$ 45.00
										</div>

										<form method="get" action="#">
					                    	<input type="number" name="cantidad" id="quantity" min="1" placeholder="Quantity" value="1">
					                    	<button type="button" class="add-to-cart">
					                    		<i class="fa fa-shopping-cart" aria-hidden="true"></i> AÑADIR A CARRITO
					                    	</button>
					                    	<a href="javascript:;" class="btn-wishlist">&nbsp;</a>
					                    </form>

					                    <p>
					                    	<a href="javascript:;">PRINT</a>  |  <a href="javascript:;" >SHARE</a>
					                    </p>
									</div>

								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
	
</section>
<section class="pagination">
	<div class="container">
		<div class="row">
			<div class="col-md-12">

			    <a href="#" class="prev-pag">&#10094;</a>
				<ul>
					<li><a href="#">1</a></li>
					<li><a href="#">2</a></li>
					<li><a href="#">3</a></li>
					<li><a href="#">4</a></li>
					<li><a href="#">5</a></li>
				</ul>
				<a href="#" class="next-pag">&#10095;</a>
				
			</div>
		</div>
	</div>
</section>
<?php get_footer( 'shop' ); ?>

<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header( 'shop' ); ?>
<section class="heading category-parent">
	<div class="container">
		<div class="row">
			<div class="col-md-12 back" style="background-image:url('<?php echo get_template_directory_uri(); ?>/img/heading-bracelets.png')">
				<h1>BRACELETS <br> <small>ELEGANCE</small></h1>
			</div>
		</div>
	</div>

</section>

<section class="products-grid" style="margin-top:0;">
	<div class="container">
		<div class="row grid-aguila">

			<div class="col-md-12">
				<div class="grid-item large-vertical">
					<img src="<?php echo get_template_directory_uri(); ?>/img/earings.png" alt="Collar Empote">
					<div class="info">
						<h2>ELEGANCE</h2>
						<a href="<?php echo site_url(); ?>/category/bracelets/elegance/" class="btn-browse"><span>SEE ALL</span></a>
					</div>
				</div>
				<div class="grid-item grid-item--width2">
					<img src="<?php echo get_template_directory_uri(); ?>/img/braceletes.png" alt="Collar Empote">
					<div class="info">
						<h2>LUXURY</h2>
						<a href="<?php echo site_url(); ?>/category/bracelets/elegance/" class="btn-browse"><span>SEE ALL</span></a>
					</div>
				</div>
				<div class="grid-item grid-item--width2 discount">
					<img src="<?php echo get_template_directory_uri(); ?>/img/promo.png" alt="Collar Empote">
					
				</div>
				
			</div>

		</div>
	</div>
</section>
<?php get_footer( 'shop' ); ?>
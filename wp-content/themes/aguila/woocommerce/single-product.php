<?php
/**
 * The Template for displaying all single products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header( 'shop' ); ?>
<?php while ( have_posts() ) : the_post(); ?>
<section class="product-detail">

	<section class="breadcrubms">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					
					<nav>
						
						<?php
							/**
							 * woocommerce_before_main_content hook.
							 *
							 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
							 * @hooked woocommerce_breadcrumb - 20
							 */
							do_action( 'woocommerce_before_main_content' );
						?>
						
					</nav>

					

				</div>

			</div>
		</div>
	</section>

	<div class="container">
		
		<div class="row">
			
			<div class="box-detail">
				<a href="#" class="link-product prev-product"></a>						
				<div class="col-md-7">
					<div class="row">
						<div class="col-md-2 col-sm-2 col-xs-2">
							<div class="thumb">
								<div>
									<img src="<?php echo get_template_directory_uri(); ?>/img/interior1-thumb.png" alt="Collar Empote">
								</div>
								<div>
									<img src="<?php echo get_template_directory_uri(); ?>/img/interior1-thumb.png" alt="Collar Empote">
								</div>
								<div>
									<img src="<?php echo get_template_directory_uri(); ?>/img/interior1-thumb.png" alt="Collar Empote">
								</div>
								<div>
									<img src="<?php echo get_template_directory_uri(); ?>/img/interior1-thumb.png" alt="Collar Empote">
								</div>
								<div>
									<img src="<?php echo get_template_directory_uri(); ?>/img/interior1-thumb.png" alt="Collar Empote">
								</div>
							</div>
						</div>
						<div class="col-md-10 col-sm-10  col-xs-10 no-padding">
							<div class="full">
								<div>
									<img src="<?php echo get_template_directory_uri(); ?>/img/interior1.png" alt="Collar Empote">
								</div>
								<div>
									<img src="<?php echo get_template_directory_uri(); ?>/img/interior1.png" alt="Collar Empote">
								</div>
								<div>
									<img src="<?php echo get_template_directory_uri(); ?>/img/interior1.png" alt="Collar Empote">
								</div>
								<div>
									<img src="<?php echo get_template_directory_uri(); ?>/img/interior1.png" alt="Collar Empote">
								</div>
								<div>
									<img src="<?php echo get_template_directory_uri(); ?>/img/interior1.png" alt="Collar Empote">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-5">
					<h3>ARETES WOO 23</h3>
					<article>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
							tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
							quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
						</p>
					</article>
					<p>
						<strong>Category:</strong> <span>Bracelets</span><br>
						<strong>ID:</strong> <span>1799</span>
					</p>

					<div class="price">
						$ 45.00
					</div>

					<form method="get" action="#">
		            	<input type="number" name="quantity" id="quantity" min="1" placeholder="Quantity" value="1">
		            	<input type="hidden" name="add-to-cart" value="<?php echo get_the_id(); ?>">
		            	<button type="submit" class="add-to-cart">
		            		<i class="fa fa-shopping-cart" aria-hidden="true"></i> AÑADIR A CARRITO
		            	</button>
		            	<a href="javascript:;" class="btn-wishlist">&nbsp;</a>
		            </form>

		            <p>
		            	<a href="javascript:;">PRINT</a>  |  <a href="javascript:;" >SHARE</a>
		            </p>
				</div>
				<a href="#" class="link-product next-product"></a>						
			</div>	

		</div>

	</div>

</section>

<section class="more-info">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<?php wc_get_template_part( 'content', 'single-product' ); ?>
			</div>
		</div>
	</div>
</section>

<section class="featured-products">
	<div class="container">
		
		<div class="row">
			<div class="col-md-12">
				<h2>Related Productos</h2>
				<div class="clear"></div>
				<div class="products">

					<div>
						<article>
							
							<img src="<?php echo get_template_directory_uri(); ?>/img/featured-1.png" alt="Collar Empote">
							<div class="hover">
								<div class="links">
									<a href="#" class="view-products">&nbsp;</a>
									<a href="#view-1" class="shop-product various">&nbsp;</a>
								</div>
							</div>

						</article>
						<a href="javascript:;" class="icon-heart pull-right">&nbsp;</a>
						<h3>COLLAR EMPOTE</h3>
						<h5>ACCESORIES</h5>
						<span class="price">$45.00</span>
						<div class="hidden">
							<div id="view-1">
								<div class="box-detail">
										
									<div class="box">
										<div class="full">
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/earings.png" alt="Collar Empote">
											</div>
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/braceletes.png" alt="Collar Empote">
											</div>
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/ring.png" alt="Collar Empote">
											</div>
										</div>
										<div class="thumb">
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/earings-thumb.png" alt="Collar Empote">
											</div>
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/braceletes-thumb.png" alt="Collar Empote">
											</div>
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/ring-thumb.png" alt="Collar Empote">
											</div>

										</div>
									</div>
									<div class="box right">
										<h3>ARETES WOO 23</h3>
										<article>
											<p>
												Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
												tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
												quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
											</p>
										</article>
										<p>
											<strong>Category:</strong> <span>Bracelets</span><br>
											<strong>ID:</strong> <span>1799</span>
										</p>

										<div class="price">
											$ 45.00
										</div>

										<form method="get" action="#">
					                    	<input type="number" name="cantidad" id="quantity" min="1" placeholder="Quantity" value="1">
					                    	<button type="button" class="add-to-cart">
					                    		<i class="fa fa-shopping-cart" aria-hidden="true"></i> AÑADIR A CARRITO
					                    	</button>
					                    	<a href="javascript:;" class="btn-wishlist">&nbsp;</a>
					                    </form>

					                    <p>
					                    	<a href="javascript:;">PRINT</a>  |  <a href="javascript:;" >SHARE</a>
					                    </p>
									</div>

								</div>
							</div>
						</div>
					</div>

					<div>
						<article>
							
							<img src="<?php echo get_template_directory_uri(); ?>/img/featured-2.png" alt="Collar Empote">
							<div class="hover">
								<div class="links">
									<a href="#" class="view-products">&nbsp;</a>
									<a href="#view-2" class="shop-product various">&nbsp;</a>
								</div>
							</div>

						</article>
						<a href="javascript:;" class="icon-heart pull-right">&nbsp;</a>
						<h3>COLLAR EMPOTE</h3>
						<h5>ACCESORIES</h5>
						<span class="price">$45.00</span>
						<div class="hidden">
							<div id="view-2">
								<div class="box-detail">
										
									<div class="box">
										<div class="full">
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/earings.png" alt="Collar Empote">
											</div>
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/braceletes.png" alt="Collar Empote">
											</div>
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/ring.png" alt="Collar Empote">
											</div>
										</div>
										<div class="thumb">
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/earings-thumb.png" alt="Collar Empote">
											</div>
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/braceletes-thumb.png" alt="Collar Empote">
											</div>
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/ring-thumb.png" alt="Collar Empote">
											</div>

										</div>
									</div>
									<div class="box right">
										<h3>ARETES WOO 23</h3>
										<article>
											<p>
												Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
												tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
												quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
											</p>
										</article>
										<p>
											<strong>Category:</strong> <span>Bracelets</span><br>
											<strong>ID:</strong> <span>1799</span>
										</p>

										<div class="price">
											$ 45.00
										</div>

										<form method="get" action="#">
					                    	<input type="number" name="cantidad" id="quantity" min="1" placeholder="Quantity" value="1">
					                    	<button type="button" class="add-to-cart">
					                    		<i class="fa fa-shopping-cart" aria-hidden="true"></i> AÑADIR A CARRITO
					                    	</button>
					                    	<a href="javascript:;" class="btn-wishlist">&nbsp;</a>
					                    </form>

					                    <p>
					                    	<a href="javascript:;">PRINT</a>  |  <a href="javascript:;" >SHARE</a>
					                    </p>
									</div>

								</div>
							</div>
						</div>
					</div>

					<div>
						<article>
							
							<img src="<?php echo get_template_directory_uri(); ?>/img/featured-1.png" alt="Collar Empote">
							<div class="hover">
								<div class="links">
									<a href="#" class="view-products">&nbsp;</a>
									<a href="#view-3" class="shop-product various">&nbsp;</a>
								</div>
							</div>

						</article>
						<a href="javascript:;" class="icon-heart pull-right">&nbsp;</a>
						<h3>COLLAR EMPOTE</h3>
						<h5>ACCESORIES</h5>
						<span class="price">$45.00</span>
						<div class="hidden">
							<div id="view-3">
								<div class="box-detail">
										
									<div class="box">
										<div class="full">
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/earings.png" alt="Collar Empote">
											</div>
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/braceletes.png" alt="Collar Empote">
											</div>
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/ring.png" alt="Collar Empote">
											</div>
										</div>
										<div class="thumb">
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/earings-thumb.png" alt="Collar Empote">
											</div>
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/braceletes-thumb.png" alt="Collar Empote">
											</div>
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/ring-thumb.png" alt="Collar Empote">
											</div>

										</div>
									</div>
									<div class="box right">
										<h3>ARETES WOO 23</h3>
										<article>
											<p>
												Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
												tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
												quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
											</p>
										</article>
										<p>
											<strong>Category:</strong> <span>Bracelets</span><br>
											<strong>ID:</strong> <span>1799</span>
										</p>

										<div class="price">
											$ 45.00
										</div>

										<form method="get" action="#">
					                    	<input type="number" name="cantidad" id="quantity" min="1" placeholder="Quantity" value="1">
					                    	<button type="button" class="add-to-cart">
					                    		<i class="fa fa-shopping-cart" aria-hidden="true"></i> AÑADIR A CARRITO
					                    	</button>
					                    	<a href="javascript:;" class="btn-wishlist">&nbsp;</a>
					                    </form>

					                    <p>
					                    	<a href="javascript:;">PRINT</a>  |  <a href="javascript:;" >SHARE</a>
					                    </p>
									</div>

								</div>
							</div>
						</div>
					</div>

					<div>
						<article>
							
							<img src="<?php echo get_template_directory_uri(); ?>/img/featured-2.png" alt="Collar Empote">
							<div class="hover">
								<div class="links">
									<a href="#" class="view-products">&nbsp;</a>
									<a href="#view-4" class="shop-product various">&nbsp;</a>
								</div>
							</div>

						</article>
						<a href="javascript:;" class="icon-heart pull-right">&nbsp;</a>
						<h3>COLLAR EMPOTE</h3>
						<h5>ACCESORIES</h5>
						<span class="price">$45.00</span>
						<div class="hidden">
							<div id="view-4">
								<div class="box-detail">
										
									<div class="box">
										<div class="full">
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/earings.png" alt="Collar Empote">
											</div>
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/braceletes.png" alt="Collar Empote">
											</div>
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/ring.png" alt="Collar Empote">
											</div>
										</div>
										<div class="thumb">
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/earings-thumb.png" alt="Collar Empote">
											</div>
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/braceletes-thumb.png" alt="Collar Empote">
											</div>
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/ring-thumb.png" alt="Collar Empote">
											</div>

										</div>
									</div>
									<div class="box right">
										<h3>ARETES WOO 23</h3>
										<article>
											<p>
												Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
												tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
												quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
											</p>
										</article>
										<p>
											<strong>Category:</strong> <span>Bracelets</span><br>
											<strong>ID:</strong> <span>1799</span>
										</p>

										<div class="price">
											$ 45.00
										</div>

										<form method="get" action="#">
					                    	<input type="number" name="cantidad" id="quantity" min="1" placeholder="Quantity" value="1">
					                    	<button type="button" class="add-to-cart">
					                    		<i class="fa fa-shopping-cart" aria-hidden="true"></i> AÑADIR A CARRITO
					                    	</button>
					                    	<a href="javascript:;" class="btn-wishlist">&nbsp;</a>
					                    </form>

					                    <p>
					                    	<a href="javascript:;">PRINT</a>  |  <a href="javascript:;" >SHARE</a>
					                    </p>
									</div>

								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</section>
		


<?php endwhile; // end of the loop. ?>
<?php get_footer( 'shop' ); ?>

<?php get_header( ); ?>
<?php while ( have_posts() ) : the_post(); ?>
<section class="heading how-do" style="background:url('<?php echo get_template_directory_uri(); ?>/img/how-do.jpg') no-repeat 50% 50% #000;">
	<div class="container">
		<div class="row">
			<div class="col-md-12" >
				<h1>&nbsp;</h1>
				
				<p>&nbsp;</p>
			</div>
		</div>
	</div>

</section>
<section class="contact-page page-normal">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				
				<?php the_content();  ?>	

			</div>
		</div>
	</div>
</section>

<?php endwhile; // end of the loop. ?>

<?php get_footer(); ?>
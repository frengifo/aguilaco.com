<?php 

	

	add_theme_support( 'post-thumbnails', array( 'productos', 'servicios', 'post', 'page', 'clientes' ) );



	function register_my_menu() {

	  register_nav_menu('header-menu',__( 'Header Menu' ));

	}

	add_action( 'init', 'register_my_menu' );


	function aguila_wp_title( $title, $sep ) {
		global $paged, $page;

		if ( is_feed() ) {
			return $title;
		}

		// Add the site name.
		$title .= get_bloginfo( 'name', 'display' );

		// Add the site description for the home/front page.
		$site_description = get_bloginfo( 'description', 'display' );
		if ( $site_description && ( is_home() || is_front_page() ) ) {
			$title = "$title $sep $site_description";
		}

		// Add a page number if necessary.
		if ( ( $paged >= 2 || $page >= 2 ) && ! is_404() ) {
			$title = "$title $sep " . sprintf( __( 'Page %s', 'aguila' ), max( $paged, $page ) );
		}

		return $title;
	}
	add_filter( 'wp_title', 'aguila_wp_title', 10, 2 );


	function aguila_scripts() {

	    //if ( is_front_page() ) {

			// Add Genericons, used in the main stylesheet.

			wp_enqueue_style( 'fancybox-style', get_template_directory_uri() . '/js/vendor/fancybox/source/jquery.fancybox.css', array(), '2.1.5' );

			wp_enqueue_script( 'fancybox-script', get_template_directory_uri() . '/js/vendor/fancybox/source/jquery.fancybox.pack.js', array(), '2.1.5');

			// Get the post type from the query

		//}

	}

	add_action( 'wp_enqueue_scripts', 'aguila_scripts' );


	/***************************************/
	add_action( 'after_setup_theme', 'woocommerce_support' );
	function woocommerce_support() {
	    add_theme_support( 'woocommerce' );
	}

 ?>
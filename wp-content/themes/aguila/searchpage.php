<?php
/*
Template Name: Search Page
*/
?>
<?php get_header(); ?>

<section class="content-page searchpage blog">
        
    <section class="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                	<?php if ( function_exists('yoast_breadcrumb') ) {
                            yoast_breadcrumb('
                            <p id="breadcrumbs" class="pull-right">','</p>
                            ');
                        }
                    ?>
                    <h2> <span></span> Busqueda con la palabra: "<?php echo $_GET['search']; ?>"</h2>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="container">
            <div class="row">


            	<div class="col-md-3 col-md-offset-9">
                    <form class="find-post" method="get" action="<?php echo site_url(); ?>/searchpage/">
                        <input type="text" name="search" placeholder="Busca una Noticia" />
                        <button type="submit"></button>
                    </form>
                </div>

               <div class="clear"></div>
               <section class="col-md-9 list-posts">
               		
               		<?php 

               		$args = array(  
					'post_status' => 'publish',
					'post_type' => 'post',
					's' =>  isset($_GET['search']) ? $_GET['search']:"" );

               		$search = new WP_Query($args);
               		?>
               		<?php $i=1; ?>
               		 <?php while ( $search->have_posts() ) : $search->the_post(); ?>
               		 	<article class="col-md-6">
                            <figure>
                                <a href="<?php the_permalink(); ?>" class="box-img">
                                    <?php the_post_thumbnail(); ?>
                                </a>
                                <figcaption>
                                    <h2>
                                        <a href="<?php the_permalink(); ?>">
                                            <?php the_title(); ?>
                                        </a>
                                    </h2>
                                    <div class="date">
                                        <span><?php the_date(); ?> </span> | por <?php the_author(); ?> 
                                    </div>
                                    <div class="excerpt">
                                        <?php echo excerpt(40); ?>
                                    </div>
                                    <a href="<?php the_permalink(); ?>" class="btn-vermas">LEER MÁS</a>
                                </figcaption>

                            </figure>
                        </article>
                        <?php if ($i % 2 == 0): ?>
                            <div class="clear"></div>
                        <?php endif ?>
                        <?php $i++; ?>
               		 <?php endwhile; ?>
               
               </section>
               <?php get_template_part( 'content', 'aside-blog' ); ?>
            </div>
        </div>                                  
    </section>
</section>

<?php get_footer(); ?>
<?php get_header( ); ?>
<?php while ( have_posts() ) : the_post(); ?>

<section class="contact-page checkout">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				
				<?php the_content();  ?>	

			</div>
		</div>
	</div>
</section>

<?php endwhile; // end of the loop. ?>

<?php get_footer(); ?>
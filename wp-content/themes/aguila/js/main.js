jQuery(document).ready(function( $ ){ 



    //Slider
    var slider = $(".slider .box").unslider({
      //animation: 'fade',
  		nav : true,
  		arrows: false,
  		autoplay:true,
  		delay:6000,

  	});

    $(".menu-info").on("click", function(){
    	$(this).next().slideToggle();
    })

    $('.featured-products .products').slick({
      slidesToShow: 3,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 2
          }
        }
      ]
    });


    

    if( $("#map").length > 0 ){
  		initMap();
  	}

    if ( $(".featured-video-plus").length > 0 ) {
        $(".featured-video-plus").parent().addClass("remove-trans");
    };

    $('.featured-products .box-detail .full').slick({
    
      arrows: false,
      asNavFor: '.thumb',
      slidesToShow: 1,
      fade: true,
      width   : '70%',
    });
    $('.featured-products .box-detail .thumb').slick({
      arrows: false,
      vertical:true,
      asNavFor: '.full',
      slidesToShow: 6,                                                                                                                                                   
      slidesToScroll: 1,
      focusOnSelect: true
    });

    /*LISTADO DE PRODUCTOS*/
    $('.list-products .box-detail .full').slick({
    
      arrows: false,
      asNavFor: '.thumb',
      slidesToShow: 1,
      fade: true,
      width   : '70%',
    });
    $('.list-products .box-detail .thumb').slick({
      arrows: false,
      vertical:true,
      asNavFor: '.full',
      slidesToShow: 6,
      slidesToScroll: 1,
      focusOnSelect: true
    });

    /*PRODUCTO DETALLE*/
    $('.product-detail .box-detail .full').slick({
    
      arrows: false,
      asNavFor: '.thumb',
      slidesToShow: 1,
      fade: true,
      width   : '70%',
    });
    $('.product-detail .box-detail .thumb').slick({
      arrows: false,
      vertical:true,
      asNavFor: '.full',
      slidesToShow: 5,
      slidesToScroll: 1,
      focusOnSelect: true
    });


    if ( $(".various").length > 0 ) {
      $(".various").fancybox({
        padding: 0,
        fitToView : true,
        maxHeight : 550,
        width   : '850px',
        height    : '550px',
        openEffect  : 'none',
        closeEffect : 'none',
        autoSize  : false,
        closeClick  : false,
        beforeShow : function(){
          
        }
      });
    }

    $(window).on('scroll', function(){

      if ($(window).width() > 992) {

        if ( $(window).scrollTop() > 50 ){
          $('.top').addClass('menu-fixed');
          $('header').addClass('fix-padding-top');
          $(".menu-container").slideUp();
        } else {
          $('.top').removeClass('menu-fixed');
          $(".menu-container").slideDown();
          $('header').removeClass('fix-padding-top');
        }

      }

    });

    $(".btn-search").on("click", function(){

      if ( $(this).hasClass('showing') ) {
        $(this).prev().fadeOut();
      }else{
        $(this).prev().fadeIn();
      }
      
      $(this).toggleClass("showing");

    })

    $('#nav-icon3').click(function(){
      $(this).toggleClass('open');
      $(this).next().slideToggle();
    });

    $(window).click(function() {
      
      $(".menu-responsive nav.list-menu").slideUp();
      $("#nav-icon3").removeClass('open');
      $(".btn-search").removeClass("showing");
      $(".search-input").fadeOut();

    });

    $(".menu-responsive nav.list-menu, #nav-icon3, .btn-search, .search-input").click(function(event){
        event.stopPropagation();
    });
})

function initMap() {

  var park = {lat: -12.0725831, lng: -76.9911425};

  // Create a map object and specify the DOM element for display.
  var map = new google.maps.Map(document.getElementById('map'), {
    center: park,
    scrollwheel: false,
    zoom: 18,
    scrollwheel: false,
    draggable: false
  });


  var contentString = '<div id="content">'+
      '<div id="siteNotice">'+
      '</div>'+
      '<h1 id="firstHeading" class="firstHeading">PERTEL</h1>'+
      '<div id="bodyContent">'+
      '<div> <p>Calle Mariscal Andrés de Santa Cruz 202/208 </p> <p>San Luis. Lima – Peru</p> <p> <a href="#">(01) 3267755</a> </p></div>'+
      '</div>'+
      '</div>';

  var infowindow = new google.maps.InfoWindow({
    content: contentString
  });

  var marker = new google.maps.Marker({
    map: map,
    position: park,
    title: 'Hello World!',
    icon:site_url+'/wp-content/themes/pertel/img/marker-map.png',
  });

  marker.addListener('click', function() {
    infowindow.open(map, marker);
  });

  


}
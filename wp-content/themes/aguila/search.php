<?php

get_header(); ?>

	<section>
		<p>&nbsp;</p>
		<div class="container" >
			<div class="row">
				<?php if ( have_posts() ) : ?>
						<?php $i=1; ?>
						<?php while ( have_posts() ) : the_post();  ?>


								<div class="col-md-6 col-sm-6 col-xs-6 aside-latest-post">
									<aside class="row">


										<article class="col-md-12">
											<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
											<p>&nbsp;</p>

											<figure>
												<a href="<?php the_permalink(); ?>"> <?php the_post_thumbnail('full'); ?> </a>
											</figure>
										</article>


										
									</aside>
								</div>	
								<?php if ( $i % 2 == 0) { ?>
									<div class="clear"></div>
								<?php } ?>
								<?php $i++ ?>
						<?php endwhile;
						

					else :
						// If no content, include the "No posts found" template.
						echo "No se encontraron coincidencias.";

					endif;
				?>
			</div>
		</div><!-- #content -->
	</section><!-- #primary -->

<?php
get_footer();

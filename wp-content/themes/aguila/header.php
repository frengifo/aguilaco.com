<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width">
    <title><?php wp_title( '|', true, 'right' ); ?></title>
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/main.css">
    <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">-->
    <!--<link href="https://fonts.googleapis.com/css?family=Josefin+Sans:300|Open+Sans:300" rel="stylesheet">-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300" rel="stylesheet">
    <script src="<?php echo get_template_directory_uri() ?>/js/vendor/modernizr-2.8.3.min.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
    <!--[if lt IE 9]>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5.js"></script>
    <![endif]-->
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<header>

		<section class="top">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<a href="<?php echo site_url(); ?>" class="logo pull-left">
							<img src="<?php echo get_template_directory_uri(); ?>/img/logo-aguila.png" alt="Aguila Co." />
						</a>
						<div class="socials pull-left">
							<a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/tw.png" alt="Aguila Co." /></a> 
							<a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/fb.png" alt="Aguila Co." /></a>
						</div>
						<div class="info pull-right">
							<a href="<?php echo site_url(); ?>/my-account" class="lnk">My Account</a>
							<span class="sep"></span>
							<a href="<?php echo site_url(); ?>/my-wishlist/" class="lnk">Wishlist</a>
							<div  class="car-top-info">
								<img src="<?php echo get_template_directory_uri(); ?>/img/icon-cart.png" alt="Aguila Co." />
								<sup>0</sup>
								<span class="list-mini-cart">
									<ul >
										<li>
											<img src="<?php echo get_template_directory_uri(); ?>/img/earings-thumb.png" class="pull-left">
											<p>
												BRAZALETE<br>
												<span class="price">$ 45.00</span>
											</p>
										</li>
										<li>
											<img src="<?php echo get_template_directory_uri(); ?>/img/earings-thumb.png" class="pull-left">
											<p>
												BRAZALETE<br>
												<span class="price">$ 45.00</span>
											</p>
										</li>
										<li>
											<img src="<?php echo get_template_directory_uri(); ?>/img/earings-thumb.png" class="pull-left">
											<p>
												BRAZALETE<br>
												<span class="price">$ 45.00</span>
											</p>
										</li>
									</ul>
									<span class="subtotal pull-right">$ 120.00</span>
									<p>SUBTOTAL</p>
									<a href="<?php echo site_url(); ?>/cart/" class="btn-view-cart">VIEW CART</a>
								</span>
							</div>
							<form action="#" method="post">
								<input type="text" name="search" class="search-input"> 
								<button type="button" class="btn-search"><img src="<?php echo get_template_directory_uri(); ?>/img/icon-search.png" alt="Aguila Co." /></button>
							</form>
							<div class="menu-responsive">
								<div id="nav-icon3">
								  <span></span>
								  <span></span>
								  <span></span>
								  <span></span>
								</div>
								<nav class="list-menu" style="display:none">
									<?php wp_nav_menu( array("menu" => "menu-principal") ); ?>
								</nav>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section class="menu-container">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<a href="<?php echo site_url(); ?>" class="logo">
							<img src="<?php echo get_template_directory_uri(); ?>/img/logo-aguila.png" alt="Aguila Co." />
						</a>
						<div class="clear"></div> 
						<nav class="list-menu">
							<?php wp_nav_menu( array("menu" => "menu-principal") ); ?>
						</nav>
					</div>
				</div>
			</div>
		</section>
	</header>
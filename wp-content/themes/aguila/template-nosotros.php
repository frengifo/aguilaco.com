<?php 
/**
 * Template Name: Nosotros
 *
 */
?>
<?php get_header(); ?>
<?php while ( have_posts() ) : the_post(); ?>
<section class="content-page nosotros">
        
    <section class="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    <?php if ( function_exists('yoast_breadcrumb') ) {
                            yoast_breadcrumb('
                            <p id="breadcrumbs" class="pull-right">','</p>
                            ');
                        }
                    ?>
                    <h2> <span><img src="<?php echo get_template_directory_uri() ?>/img/icon-nosotros.png"></span> <?php the_title(); ?></h2>
                    
                </div>
            </div>
        </div>
    </section>
    <section class="banner" style="background-image:url('<?php the_field('imagen_de_banner') ?>');">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <article>
                        <h1>
                            <?php the_field('texto') ?>
                        </h1>
                    </article>
                </div>

            </div>
        </div>
    </section>
    <section class="contenido">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <article>
                    	<?php
	                    	$thumbnail_id = get_post_thumbnail_id();
							$thumb = wp_get_attachment_image_src($thumbnail_id,'full', true);
							
                    	 ?> 
                        <?php if ( trim($thumb[0]) != "" ): ?>
                        	<p class="featured-image">	
	                            <img src="<?php echo $thumb[0]; ?>" class="alignleft">
	                        </p>
                        <?php endif ?>
                        <?php the_content(); ?>

                    </article>
                    <div class="clear"></div>
                    <section class="orientacion">
                    <?php

                    $tipos = get_terms( 'orientacion', array(  'hide_empty' => 0, 'order' => 'ASC'));
            		
					 	if ( ! empty( $tipos ) && ! is_wp_error( $tipos ) ){
					    
						    foreach ( $tipos as $term ) { ?>

	                        <div class="box" style="background-color:<?php the_field('color_o', $term); ?>;">
	                            <h2><?php echo $term->name; ?></h2>
	                            <p>
	                              <?php echo $term->description; ?>
	                            </p>
	                        </div>
	                    <?php } ?>
                    <?php } ?>
                    </section>

                    <div class="politica">
                        <h2>POLÍTICAS</h2>
                        <div class="excerpt">
                            <?php the_field('politicas_desripcion'); ?>
                        </div>
                        <div class="row">
                        	<?php

		                    $tipos = get_terms( 'politicas', array( 'hide_empty' => 0 ) );
		            		$i=1;
							if ( ! empty( $tipos ) && ! is_wp_error( $tipos ) ){
							    
								foreach ( $tipos as $term ) { ?>

		                            <div class="col-md-6 col-sm-6 col-xs-12 box">
		                                <div class="row">
		                                    <div class="col-md-3 col-sm-3 col-xs-12">
		                                        <img src="<?php the_field('icono_politicas', $term); ?>">
		                                    </div>
		                                    <div class="col-md-9 col-sm-9 col-xs-12">
		                                        <h2><?php echo $term->name; ?></h2>
		                                        <article>
		                                            <p>
		                                                <?php echo $term->description; ?>
		                                            </p>
		                                        </article>
		                                    </div>
		                                </div>
		                            </div>
		                            <?php if ($i % 2 == 0): ?>
		                            	<div class="clear"></div>
		                            <?php endif ?>
			                    	<?php $i++; ?>
			                    <?php } ?>

		                	<?php } ?>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
</section>
<?php endwhile; ?>
<?php get_footer(); ?>
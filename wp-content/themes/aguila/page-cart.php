<?php get_header(); ?>
<section class="heading my-cart" style="background:url('<?php echo get_template_directory_uri(); ?>/img/back-my-cart.png') no-repeat 50% 50% #000;">
	<div class="container">
		<div class="row">
			<div class="col-md-12" >
				<h1>&nbsp;</h1>
				
				<p>&nbsp;</p>
			</div>
		</div>
	</div>

</section>
<?php while ( have_posts() ) : the_post(); ?>
<section class="cart-page">
	<div class="container">
		<div class="row">
			<div class="row">
				<div class="col-md-12">
					<?php

						wc_print_notices();

						do_action( 'woocommerce_before_cart' ); ?>

						<form action="<?php echo esc_url( wc_get_cart_url() ); ?>" method="post">

						<?php do_action( 'woocommerce_before_cart_table' ); ?>

						<table class="shop_table shop_table_responsive cart" cellspacing="0">
							<thead>
								<tr>
									
									<th class="product-thumbnail">&nbsp;</th>
									<th class="product-name"><?php _e( 'Product', 'woocommerce' ); ?></th>
									<th class="product-price"><?php _e( 'Price', 'woocommerce' ); ?></th>
									<th class="product-quantity"><?php _e( 'Quantity', 'woocommerce' ); ?></th>
									<th class="product-subtotal"><?php _e( 'Total', 'woocommerce' ); ?></th>
									<th class="product-remove">&nbsp;</th>
								</tr>
							</thead>
							<tbody>
								<?php do_action( 'woocommerce_before_cart_contents' ); ?>
								<!--<tr>
									<td class="product-thumbnail">
										<img src="<?php echo get_template_directory_uri(); ?>/img/braceletes.png" width="80" alt="bracelets">
									</td>
									<td class="product-name">
										BRAZELETE ADOS
									</td>
									<td class="product-price" data-title="<?php _e( 'Price', 'woocommerce' ); ?>">
										<span class="price">$ 45.00</span>
									</td>
									<td class="product-quantity" data-title="<?php _e( 'Quantity', 'woocommerce' ); ?>">
										<input type="number" name="" min="1" size="2" value="3">
									</td>
									<td class="product-subtotal" data-title="<?php _e( 'Total', 'woocommerce' ); ?>">
										<span>$ 45.00</span>
									</td>
									<td class="product-remove">
										<a href="#">&times;</a>
									</td>
								</tr>
								<tr>
									<td class="product-thumbnail">
										<img src="<?php echo get_template_directory_uri(); ?>/img/braceletes.png" width="80" alt="bracelets">
									</td>
									<td class="product-name">
										BRAZELETE ADOS
									</td>
									<td class="product-price" data-title="<?php _e( 'Price', 'woocommerce' ); ?>">
										<span class="price">$ 45.00</span>
									</td>
									<td class="product-quantity" data-title="<?php _e( 'Quantity', 'woocommerce' ); ?>">
										<input type="number" name="" min="1" size="2" value="3">
									</td>
									<td class="product-subtotal" data-title="<?php _e( 'Total', 'woocommerce' ); ?>">
										<span>$ 45.00</span>
									</td>
									<td class="product-remove">
										<a href="#">&times;</a>
									</td>
								</tr>
								<tr>
									<td class="product-thumbnail">
										<img src="<?php echo get_template_directory_uri(); ?>/img/braceletes.png" width="80" alt="bracelets">
									</td>
									<td class="product-name">
										BRAZELETE ADOS
									</td>
									<td class="product-price" data-title="<?php _e( 'Price', 'woocommerce' ); ?>">
										<span class="price">$ 45.00</span>
									</td>
									<td class="product-quantity" data-title="<?php _e( 'Quantity', 'woocommerce' ); ?>">
										<input type="number" name="" min="1" size="2" value="3">
									</td>
									<td class="product-subtotal" data-title="<?php _e( 'Total', 'woocommerce' ); ?>">
										<span>$ 45.00</span>
									</td>
									<td class="product-remove">
										<a href="#">&times;</a>
									</td>
								</tr>-->
								<?php
								foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
									$_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
									$product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

									if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
										$product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
										?>
										<tr class="<?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>">

											

											<td class="product-thumbnail">
												<?php
													$thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );

													if ( ! $product_permalink ) {
														echo $thumbnail;
													} else {
														printf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $thumbnail );
													}
												?>
											</td>

											<td class="product-name" data-title="<?php _e( 'Product', 'woocommerce' ); ?>">
												<?php
													if ( ! $product_permalink ) {
														echo apply_filters( 'woocommerce_cart_item_name', $_product->get_title(), $cart_item, $cart_item_key ) . '&nbsp;';
													} else {
														echo apply_filters( 'woocommerce_cart_item_name', sprintf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $_product->get_title() ), $cart_item, $cart_item_key );
													}

													// Meta data
													echo WC()->cart->get_item_data( $cart_item );

													// Backorder notification
													if ( $_product->backorders_require_notification() && $_product->is_on_backorder( $cart_item['quantity'] ) ) {
														echo '<p class="backorder_notification">' . esc_html__( 'Available on backorder', 'woocommerce' ) . '</p>';
													}
												?>
											</td>

											<td class="product-price" data-title="<?php _e( 'Price', 'woocommerce' ); ?>">
												<?php
													echo apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key );
												?>
											</td>

											<td class="product-quantity" data-title="<?php _e( 'Quantity', 'woocommerce' ); ?>">
												<?php
													if ( $_product->is_sold_individually() ) {
														$product_quantity = sprintf( '1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key );
													} else {
														$product_quantity = woocommerce_quantity_input( array(
															'input_name'  => "cart[{$cart_item_key}][qty]",
															'input_value' => $cart_item['quantity'],
															'max_value'   => $_product->backorders_allowed() ? '' : $_product->get_stock_quantity(),
															'min_value'   => '0'
														), $_product, false );
													}

													echo apply_filters( 'woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item );
												?>
											</td>

											<td class="product-subtotal" data-title="<?php _e( 'Total', 'woocommerce' ); ?>">
												<?php
													echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key );
												?>
											</td>


											<td class="product-remove">
												<?php
													echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
														'<a href="%s" class="remove" title="%s" data-product_id="%s" data-product_sku="%s">&times;</a>',
														esc_url( WC()->cart->get_remove_url( $cart_item_key ) ),
														__( 'Remove this item', 'woocommerce' ),
														esc_attr( $product_id ),
														esc_attr( $_product->get_sku() )
													), $cart_item_key );
												?>
											</td>

										</tr>
										<?php
									}
								}

								do_action( 'woocommerce_cart_contents' );
								?>
								<tr>
									<td colspan="6" class="buttons">
										<div class="clear"></div>

										<a href="<?php echo site_url(); ?>/category/bracelets/" class="button pull-left">CONTINUES SHOPPING</a>
										<a href="<?php echo site_url(); ?>/shop/" class="button pull-right">EMPTY CART</a>
										<input type="submit" class="button pull-right" name="update_cart" value="<?php esc_attr_e( 'Update Cart', 'woocommerce' ); ?>" />
										
									</td>
								</tr>
								<tr>
									<td colspan="6" class="actions">

										<?php if ( wc_coupons_enabled() ) { ?>
											<div class="coupon">
												<h3>DISCOUNT CODES</h3>
												<p><i>Enter your coupon code if you have one.</i></p>
												<label for="coupon_code"><?php _e( 'Coupon:', 'woocommerce' ); ?></label> <input type="text" name="coupon_code" class="input-text" id="coupon_code" value="" placeholder="<?php esc_attr_e( 'Coupon code', 'woocommerce' ); ?>" /> <input type="submit" class="button" name="apply_coupon" value="<?php esc_attr_e( 'Apply Coupon', 'woocommerce' ); ?>" />

												<?php do_action( 'woocommerce_cart_coupon' ); ?>
											</div>
										<?php } ?>

										

										<?php do_action( 'woocommerce_cart_actions' ); ?>

										<?php wp_nonce_field( 'woocommerce-cart' ); ?>

										<div class="cart_totals <?php if ( WC()->customer->has_calculated_shipping() ) echo 'calculated_shipping'; ?>">

											<?php do_action( 'woocommerce_before_cart_totals' ); ?>

											<h2><?php _e( 'Cart Totals', 'woocommerce' ); ?></h2>

											<table cellspacing="0" class="shop_table shop_table_responsive">

												<tr class="cart-subtotal">
													<th><?php _e( 'Subtotal', 'woocommerce' ); ?></th>
													<td data-title="<?php esc_attr_e( 'Subtotal', 'woocommerce' ); ?>"><?php wc_cart_totals_subtotal_html(); ?></td>
												</tr>

												<?php foreach ( WC()->cart->get_coupons() as $code => $coupon ) : ?>
													<tr class="cart-discount coupon-<?php echo esc_attr( sanitize_title( $code ) ); ?>">
														<th><?php wc_cart_totals_coupon_label( $coupon ); ?></th>
														<td data-title="<?php echo esc_attr( wc_cart_totals_coupon_label( $coupon, false ) ); ?>"><?php wc_cart_totals_coupon_html( $coupon ); ?></td>
													</tr>
												<?php endforeach; ?>

												<?php if ( WC()->cart->needs_shipping() && WC()->cart->show_shipping() ) : ?>

													<?php do_action( 'woocommerce_cart_totals_before_shipping' ); ?>

													<?php wc_cart_totals_shipping_html(); ?>

													<?php do_action( 'woocommerce_cart_totals_after_shipping' ); ?>

												<?php elseif ( WC()->cart->needs_shipping() && 'yes' === get_option( 'woocommerce_enable_shipping_calc' ) ) : ?>

													<tr class="shipping">
														<th><?php _e( 'Shipping', 'woocommerce' ); ?></th>
														<td data-title="<?php esc_attr_e( 'Shipping', 'woocommerce' ); ?>"><?php woocommerce_shipping_calculator(); ?></td>
													</tr>

												<?php endif; ?>

												<?php foreach ( WC()->cart->get_fees() as $fee ) : ?>
													<tr class="fee">
														<th><?php echo esc_html( $fee->name ); ?></th>
														<td data-title="<?php echo esc_attr( $fee->name ); ?>"><?php wc_cart_totals_fee_html( $fee ); ?></td>
													</tr>
												<?php endforeach; ?>

												<?php if ( wc_tax_enabled() && 'excl' === WC()->cart->tax_display_cart ) :
													$taxable_address = WC()->customer->get_taxable_address();
													$estimated_text  = WC()->customer->is_customer_outside_base() && ! WC()->customer->has_calculated_shipping()
															? sprintf( ' <small>(' . __( 'estimated for %s', 'woocommerce' ) . ')</small>', WC()->countries->estimated_for_prefix( $taxable_address[0] ) . WC()->countries->countries[ $taxable_address[0] ] )
															: '';

													if ( 'itemized' === get_option( 'woocommerce_tax_total_display' ) ) : ?>
														<?php foreach ( WC()->cart->get_tax_totals() as $code => $tax ) : ?>
															<tr class="tax-rate tax-rate-<?php echo sanitize_title( $code ); ?>">
																<th><?php echo esc_html( $tax->label ) . $estimated_text; ?></th>
																<td data-title="<?php echo esc_attr( $tax->label ); ?>"><?php echo wp_kses_post( $tax->formatted_amount ); ?></td>
															</tr>
														<?php endforeach; ?>
													<?php else : ?>
														<tr class="tax-total">
															<th><?php echo esc_html( WC()->countries->tax_or_vat() ) . $estimated_text; ?></th>
															<td data-title="<?php echo esc_attr( WC()->countries->tax_or_vat() ); ?>"><?php wc_cart_totals_taxes_total_html(); ?></td>
														</tr>
													<?php endif; ?>
												<?php endif; ?>

												<?php do_action( 'woocommerce_cart_totals_before_order_total' ); ?>

												<tr class="order-total">
													<th><?php _e( 'Total', 'woocommerce' ); ?></th>
													<td data-title="<?php esc_attr_e( 'Total', 'woocommerce' ); ?>"><?php wc_cart_totals_order_total_html(); ?></td>
												</tr>

												<?php do_action( 'woocommerce_cart_totals_after_order_total' ); ?>

											</table>

											<div class="wc-proceed-to-checkout">
												<?php do_action( 'woocommerce_proceed_to_checkout' ); ?>
											</div>

											<?php do_action( 'woocommerce_after_cart_totals' ); ?>

										</div>
									</td>
								</tr>

								<?php do_action( 'woocommerce_after_cart_contents' ); ?>
							</tbody>
						</table>

						<?php do_action( 'woocommerce_after_cart_table' ); ?>
								
						</form>
				</div>
			</div>
		</div>
	</div>
</section>

<?php endwhile; // end of the loop. ?>
<?php get_footer( ); ?>
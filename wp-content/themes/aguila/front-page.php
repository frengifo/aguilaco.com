<?php get_header(); ?>
<style type="text/css">

</style>
<section class="slider">
	<div class="box">
		<ul>
        	<?php $slider = new WP_Query( array(  'post_type' => 'slider' ) ); ?> 
        	<?php while ( $slider->have_posts() ) : $slider->the_post(); ?>               
            <li>
                <img src="<?php the_field('imagen') ?>" alt="<?php the_title() ?>" />
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<section class="description">
								<h2><?php the_field('description') ?></h2>
								<a href="<?php the_field('link') ?>" class="btn-black">VIEW PRODUCTS</a>
							</section>
						</div>
					</div>
				</div>
            </li>

            <?php endwhile; ?>
        </ul>
	</div>
</section>
<section class="products-grid">
	<div class="container">
		<div class="row grid-aguila">

			<div class="col-md-12">
				<div class="grid-item large-vertical">
					<img src="<?php echo get_template_directory_uri(); ?>/img/earings.png" alt="Collar Empote">
					<div class="info">
						<h2>EARRINGS</h2>
						<a href="<?php echo site_url(); ?>/category/bracelets" class="btn-browse"><span>BROWSE NOW</span></a>
					</div>
				</div>
				<div class="grid-item grid-item--width2">
					<img src="<?php echo get_template_directory_uri(); ?>/img/braceletes.png" alt="Collar Empote">
					<div class="info">
						<h2>BRACELETS</h2>
						<a href="<?php echo site_url(); ?>/category/bracelets" class="btn-browse"><span>BROWSE NOW</span></a>
					</div>
				</div>
				<div class="grid-item grid-item--width2 discount">
					<img src="<?php echo get_template_directory_uri(); ?>/img/promo.png" alt="Collar Empote">
					
				</div>
				<div class="grid-item grid-item--width2">
					<img src="<?php echo get_template_directory_uri(); ?>/img/necklaces.png" alt="Collar Empote">
					<div class="info">
						<h2>NECKLACES</h2>
						<a href="<?php echo site_url(); ?>/category/bracelets" class="btn-browse"><span>BROWSE NOW</span></a>
					</div>
				</div>
				<div class="grid-item ">
					<img src="<?php echo get_template_directory_uri(); ?>/img/ring.png" alt="Collar Empote">
					<div class="info">
						<h2>RINGS</h2>
						<a href="<?php echo site_url(); ?>/category/bracelets" class="btn-browse"><span>BROWSE NOW</span></a>
					</div>
				</div>
			</div>

		</div>
	</div>
</section>

<section class="featured-products">
	<div class="container">
		
		<div class="row">
			<div class="col-md-12">
				<h2>Featured Productos</h2>
				<div class="clear"></div>
				<div class="products">

					<div>
						<article>
							
							<img src="<?php echo get_template_directory_uri(); ?>/img/featured-1.png" alt="Collar Empote">
							<div class="hover">
								<div class="links">
									<a href="<?php echo site_url(); ?>/shop/lace-collar/" class="view-products">&nbsp;</a>
									<a href="#view-1" class="shop-product various">&nbsp;</a>
								</div>
							</div>

						</article>
						<a href="javascript:;" class="icon-heart pull-right">&nbsp;</a>
						<h3>COLLAR EMPOTE</h3>
						<h5>ACCESORIES</h5>
						<span class="price">$45.00</span>
						<div class="hidden">
							<div id="view-1">
								<div class="box-detail">
										
									<div class="box">
										<div class="full">
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/earings.png" alt="Collar Empote">
											</div>
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/braceletes.png" alt="Collar Empote">
											</div>
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/ring.png" alt="Collar Empote">
											</div>
										</div>
										<div class="thumb">
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/earings-thumb.png" alt="Collar Empote">
											</div>
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/braceletes-thumb.png" alt="Collar Empote">
											</div>
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/ring-thumb.png" alt="Collar Empote">
											</div>

										</div>
									</div>
									<div class="box right">
										<h3>ARETES WOO 23</h3>
										<article>
											<p>
												Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
												tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
												quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
											</p>
										</article>
										<p>
											<strong>Category:</strong> <span>Bracelets</span><br>
											<strong>ID:</strong> <span>1799</span>
										</p>

										<div class="price">
											$ 45.00
										</div>

										<form method="get" action="#">
					                    	<input type="number" name="cantidad" id="quantity" min="1" placeholder="Quantity" value="1">
					                    	<button type="button" class="add-to-cart">
					                    		<i class="fa fa-shopping-cart" aria-hidden="true"></i> AÑADIR A CARRITO
					                    	</button>
					                    	<a href="javascript:;" class="btn-wishlist">&nbsp;</a>
					                    </form>

					                    <p>
					                    	<a href="javascript:;">PRINT</a>  |  <a href="javascript:;" >SHARE</a>
					                    </p>
									</div>

								</div>
							</div>
						</div>
					</div>

					<div>
						<article>
							
							<img src="<?php echo get_template_directory_uri(); ?>/img/featured-2.png" alt="Collar Empote">
							<div class="hover">
								<div class="links">
									<a href="<?php echo site_url(); ?>/shop/lace-collar/" class="view-products">&nbsp;</a>
									<a href="#view-2" class="shop-product various">&nbsp;</a>
								</div>
							</div>

						</article>
						<a href="javascript:;" class="icon-heart pull-right">&nbsp;</a>
						<h3>COLLAR EMPOTE</h3>
						<h5>ACCESORIES</h5>
						<span class="price">$45.00</span>
						<div class="hidden">
							<div id="view-2">
								<div class="box-detail">
										
									<div class="box">
										<div class="full">
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/earings.png" alt="Collar Empote">
											</div>
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/braceletes.png" alt="Collar Empote">
											</div>
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/ring.png" alt="Collar Empote">
											</div>
										</div>
										<div class="thumb">
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/earings-thumb.png" alt="Collar Empote">
											</div>
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/braceletes-thumb.png" alt="Collar Empote">
											</div>
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/ring-thumb.png" alt="Collar Empote">
											</div>

										</div>
									</div>
									<div class="box right">
										<h3>ARETES WOO 23</h3>
										<article>
											<p>
												Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
												tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
												quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
											</p>
										</article>
										<p>
											<strong>Category:</strong> <span>Bracelets</span><br>
											<strong>ID:</strong> <span>1799</span>
										</p>

										<div class="price">
											$ 45.00
										</div>

										<form method="get" action="#">
					                    	<input type="number" name="cantidad" id="quantity" min="1" placeholder="Quantity" value="1">
					                    	<button type="button" class="add-to-cart">
					                    		<i class="fa fa-shopping-cart" aria-hidden="true"></i> AÑADIR A CARRITO
					                    	</button>
					                    	<a href="javascript:;" class="btn-wishlist">&nbsp;</a>
					                    </form>

					                    <p>
					                    	<a href="javascript:;">PRINT</a>  |  <a href="javascript:;" >SHARE</a>
					                    </p>
									</div>

								</div>
							</div>
						</div>
					</div>

					<div>
						<article>
							
							<img src="<?php echo get_template_directory_uri(); ?>/img/featured-1.png" alt="Collar Empote">
							<div class="hover">
								<div class="links">
									<a href="<?php echo site_url(); ?>/shop/lace-collar/" class="view-products">&nbsp;</a>
									<a href="#view-3" class="shop-product various">&nbsp;</a>
								</div>
							</div>

						</article>
						<a href="javascript:;" class="icon-heart pull-right">&nbsp;</a>
						<h3>COLLAR EMPOTE</h3>
						<h5>ACCESORIES</h5>
						<span class="price">$45.00</span>
						<div class="hidden">
							<div id="view-3">
								<div class="box-detail">
										
									<div class="box">
										<div class="full">
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/earings.png" alt="Collar Empote">
											</div>
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/braceletes.png" alt="Collar Empote">
											</div>
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/ring.png" alt="Collar Empote">
											</div>
										</div>
										<div class="thumb">
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/earings-thumb.png" alt="Collar Empote">
											</div>
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/braceletes-thumb.png" alt="Collar Empote">
											</div>
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/ring-thumb.png" alt="Collar Empote">
											</div>

										</div>
									</div>
									<div class="box right">
										<h3>ARETES WOO 23</h3>
										<article>
											<p>
												Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
												tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
												quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
											</p>
										</article>
										<p>
											<strong>Category:</strong> <span>Bracelets</span><br>
											<strong>ID:</strong> <span>1799</span>
										</p>

										<div class="price">
											$ 45.00
										</div>

										<form method="get" action="#">
					                    	<input type="number" name="cantidad" id="quantity" min="1" placeholder="Quantity" value="1">
					                    	<button type="button" class="add-to-cart">
					                    		<i class="fa fa-shopping-cart" aria-hidden="true"></i> AÑADIR A CARRITO
					                    	</button>
					                    	<a href="javascript:;" class="btn-wishlist">&nbsp;</a>
					                    </form>

					                    <p>
					                    	<a href="javascript:;">PRINT</a>  |  <a href="javascript:;" >SHARE</a>
					                    </p>
									</div>

								</div>
							</div>
						</div>
					</div>

					<div>
						<article>
							
							<img src="<?php echo get_template_directory_uri(); ?>/img/featured-2.png" alt="Collar Empote">
							<div class="hover">
								<div class="links">
									<a href="<?php echo site_url(); ?>/shop/lace-collar/" class="view-products">&nbsp;</a>
									<a href="#view-4" class="shop-product various">&nbsp;</a>
								</div>
							</div>

						</article>
						<a href="javascript:;" class="icon-heart pull-right">&nbsp;</a>
						<h3>COLLAR EMPOTE</h3>
						<h5>ACCESORIES</h5>
						<span class="price">$45.00</span>
						<div class="hidden">
							<div id="view-4">
								<div class="box-detail">
										
									<div class="box">
										<div class="full">
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/earings.png" alt="Collar Empote">
											</div>
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/braceletes.png" alt="Collar Empote">
											</div>
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/ring.png" alt="Collar Empote">
											</div>
										</div>
										<div class="thumb">
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/earings-thumb.png" alt="Collar Empote">
											</div>
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/braceletes-thumb.png" alt="Collar Empote">
											</div>
											<div>
												<img src="<?php echo get_template_directory_uri(); ?>/img/ring-thumb.png" alt="Collar Empote">
											</div>

										</div>
									</div>
									<div class="box right">
										<h3>ARETES WOO 23</h3>
										<article>
											<p>
												Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
												tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
												quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
											</p>
										</article>
										<p>
											<strong>Category:</strong> <span>Bracelets</span><br>
											<strong>ID:</strong> <span>1799</span>
										</p>

										<div class="price">
											$ 45.00
										</div>

										<form method="get" action="#">
					                    	<input type="number" name="cantidad" id="quantity" min="1" placeholder="Quantity" value="1">
					                    	<button type="button" class="add-to-cart">
					                    		<i class="fa fa-shopping-cart" aria-hidden="true"></i> AÑADIR A CARRITO
					                    	</button>
					                    	<a href="javascript:;" class="btn-wishlist">&nbsp;</a>
					                    </form>

					                    <p>
					                    	<a href="javascript:;">PRINT</a>  |  <a href="javascript:;" >SHARE</a>
					                    </p>
									</div>

								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>
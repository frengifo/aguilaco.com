<?php get_header( ); ?>
<?php while ( have_posts() ) : the_post(); ?>
<section class="heading my-cart" style="background:url('<?php echo get_template_directory_uri(); ?>/img/back-my-whislist.png') no-repeat 50% 50% #000;">
	<div class="container">
		<div class="row">
			<div class="col-md-12" >
				<h1>&nbsp;</h1>
				
				<p>&nbsp;</p>
			</div>
		</div>
	</div>

</section>
<section class="cart-page">
	<div class="container">
		<div class="row">
			<div class="row">
				<div class="col-md-12">
					<?php

						wc_print_notices();

						do_action( 'woocommerce_before_cart' ); ?>

						<form action="<?php echo esc_url( wc_get_cart_url() ); ?>" method="post">

						<?php do_action( 'woocommerce_before_cart_table' ); ?>

						<table class="shop_table shop_table_responsive cart" cellspacing="0">
							<thead>
								<tr>
									<th class="product-remove">&nbsp;</th>
									<th class="product-thumbnail">&nbsp;</th>
									<th class="product-name"><?php _e( 'Product Name', 'woocommerce' ); ?></th>
									<th class="product-price"><?php _e( 'Unit Price', 'woocommerce' ); ?></th>
									<th class="product-instock">STOCK STATUS</th>
									<th class="product-add-to-cart">&nbsp;</th>
								</tr>
							</thead>
							<tbody>
								<?php do_action( 'woocommerce_before_cart_contents' ); ?>
								<tr>
									<td class="product-remove">
										<a href="#">&times;</a>
									</td>
									<td class="product-thumbnail">
										<img src="<?php echo get_template_directory_uri(); ?>/img/braceletes.png" width="80" alt="bracelets">
									</td>
									<td class="product-name">
										BRAZELETE ADOS
									</td>
									<td class="product-price" data-title="<?php _e( 'Price', 'woocommerce' ); ?>">
										<span class="price">$ 45.00</span>
									</td>
									<td class="product-instock" >
										In Stock
									</td>
									<td class="product-add-to-cart" data-title="<?php _e( 'Total', 'woocommerce' ); ?>">
										<a href="#" class="btn-add-to-cart">ADD TO CART</a>
									</td>
									
								</tr>
								<tr>
									<td class="product-remove">
										<a href="#">&times;</a>
									</td>
									<td class="product-thumbnail">
										<img src="<?php echo get_template_directory_uri(); ?>/img/braceletes.png" width="80" alt="bracelets">
									</td>
									<td class="product-name">
										BRAZELETE ADOS
									</td>
									<td class="product-price" data-title="<?php _e( 'Price', 'woocommerce' ); ?>">
										<span class="price">$ 45.00</span>
									</td>
									<td class="product-instock" >
										In Stock
									</td>
									<td class="product-add-to-cart" data-title="<?php _e( 'Total', 'woocommerce' ); ?>">
										<a href="#" class="btn-add-to-cart">ADD TO CART</a>
									</td>
									
								</tr>
								<tr>
									<td class="product-remove">
										<a href="#">&times;</a>
									</td>
									<td class="product-thumbnail">
										<img src="<?php echo get_template_directory_uri(); ?>/img/braceletes.png" width="80" alt="bracelets">
									</td>
									<td class="product-name">
										BRAZELETE ADOS
									</td>
									<td class="product-price" data-title="<?php _e( 'Price', 'woocommerce' ); ?>">
										<span class="price">$ 45.00</span>
									</td>
									<td class="product-instock" >
										In Stock
									</td>
									<td class="product-add-to-cart" data-title="<?php _e( 'Total', 'woocommerce' ); ?>">
										<a href="#" class="btn-add-to-cart">ADD TO CART</a>
									</td>
									
								</tr>

								<?php do_action( 'woocommerce_after_cart_contents' ); ?>
							</tbody>
						</table>

						<?php do_action( 'woocommerce_after_cart_table' ); ?>
								
						</form>
						<div class="socials pull-left text-center">
							<p>Share On:</p>
							<a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/tw.png" alt="Aguila Co." /></a> 
							<a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/fb.png" alt="Aguila Co." /></a>
						</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php endwhile; // end of the loop. ?>

<?php get_footer(); ?>
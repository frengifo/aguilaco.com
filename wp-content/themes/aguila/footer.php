        <footer>
            
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-12 center">
                        
                        <a href="<?php echo site_url(); ?>">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/logo-black.png" alt="Aguila Co." />
                        </a>
                        <article>
                            Contrary to popular belief, Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                        </article>
                        <div class="socials">
                            <a href="#">
                                <img src="<?php echo get_template_directory_uri(); ?>/img/tw-black.png" alt="Aguila Co." />
                            </a>
                            <a href="#">
                                <img src="<?php echo get_template_directory_uri(); ?>/img/fb-black.png" alt="Aguila Co." />
                            </a>
                        </div>

                        <div class="suscriber">
                            <p>Sing up to get the latest news</p>
                            <form method="post" action="#">
                                <input type="email" name="email" placeholder="Enter email Adress" />
                            </form>
                        </div>
                    </div>
                    <div class="col-md-8 col-sm-12 left-box">
                        <div class="row">
                            <div class="col-md-3 col-sm-3 col-xs-6">
                                <h4>INFORMATION</h4>
                                <ul>
                                    <li>
                                        <a href="#">Email Customer Service</a>
                                    </li>
                                    <li>
                                        <a href="#">Frequently Asked Questions</a>
                                    </li>
                                    <li>
                                        <a href="#">Product Care</a>
                                    </li>
                                    <li>
                                        <a href="#">Privacy Policy</a>
                                    </li>
                                    <li>
                                        <a href="#">Cookies Policy</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-6">
                                <h4>ABOUT</h4>
                                <ul>
                                    <li>
                                        <a href="#">Corporate Responsability</a>
                                    </li>
                                    <li>
                                        <a href="#">Investors</a>
                                    </li>
                                    <li>
                                        <a href="#">Accesibility</a>
                                    </li>
                                    
                                </ul>
                            </div>
                            <div class="clear"></div>
                            <div class="col-md-3 col-sm-3 col-xs-6">
                                <h4>SHOPPING GUIDE</h4>
                                <ul>
                                    <li>
                                        <a href="#">My Account</a>
                                    </li>
                                    <li>
                                        <a href="#">Checkout</a>
                                    </li>
                                    <li>
                                        <a href="#">Wishlist</a>
                                    </li>
                                    <li>
                                        <a href="#">Contact us</a>
                                    </li>
                                    <li>
                                        <a href="#">Faq page</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-6">
                                <h4>ACCOUNT</h4>
                                <ul>
                                    <li>
                                        <a href="#">Login</a>
                                    </li>
                                    <li>
                                        <a href="#">Cart</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="clear"></div>
                    <div class="col-md-12">
                        <div class="targets pull-right">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/icon-paypal.png" alt="PayPal" >
                            <img src="<?php echo get_template_directory_uri(); ?>/img/icon-visa.png" alt="VISA" >
                            <img src="<?php echo get_template_directory_uri(); ?>/img/icon-mastercard.png" alt="MASTER CARD" >
                            <img src="<?php echo get_template_directory_uri(); ?>/img/icon-maestro.png" alt="MAESTRO" >
                            <img src="<?php echo get_template_directory_uri(); ?>/img/icon-discover.png" alt="Discover" >
                        </div>
                        <p class="copy">Copyright 2016 - Aguila & CO - ALL RIGHTS RESERVED</p>
                    </div>
                </div>
            </div>  

        </footer>
        <?php wp_footer(); ?>
        <script src="https://npmcdn.com/masonry-layout@4.1/dist/masonry.pkgd.min.js"></script>
        <script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/js/plugins.js"></script>
        <script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/js/main.js"></script>
        <script type="text/javascript">var site_url = "<?php echo site_url(); ?>";</script>
    </body>
</html>